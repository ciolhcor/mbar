mbar
===

Mbar, magic bar, est une bar de recherche permettant d'accéder à vos differents sites favoris depuis une seul et unique bar de recherche.
Mbar gère l'autocompletion.

Exemples
========

Si vous tapez "cff Genève-Aéroport > Lausanne" il vous redirigera directement sur
le site des cff avec l'horaire de train Genève-Aéroport - Lausanne.

Si vous tapez "sbb Genève-Aéroport > Laus" il vous proposera "Lausanne", "Lausanne-Flon", "Lausen", etc.