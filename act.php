<?php

define('CACHE_S', 600);

$mbar = false;
$msugg = false;
if(isset($_POST['mbar'])) $mbar=$_POST['mbar'];
elseif(isset($_GET['mbar'])) $mbar=$_GET['mbar'];
elseif(isset($_POST['msugg'])) $msugg=$_POST['msugg'];
elseif(isset($_GET['msugg'])) $msugg=$_GET['msugg'];
else {
  header('Location: index.php');
  exit;
}

abstract class act {
  static private $lstClass = [];
  static private $lstClass_sugg = [];
  static private $lstPrefix = [];
  protected $regexId = null;
  protected $regexMatches = [];
  
  // abstract static public function addCls();
  final private function __construct($id, $matches)
  {
    $this->regexId = $id;
    $this->regexMatches = $matches;
  }
  #abstract public function exec();
  #abstract public function sugg();
  public function exec() { $this->{__FUNCTION__.'_'.$this->regexId}(); }
  public function sugg() { return $this->{__FUNCTION__.'_'.$this->regexId}(); }

  static protected function addAct($cls, $arr_prefixe, $arr_Regex, $arr_Regex_sugg)
  {
    self::$lstClass[$cls] = $arr_Regex;
    self::$lstClass_sugg[$cls] = $arr_Regex_sugg;
    self::$lstPrefix = array_merge(self::$lstPrefix, $arr_prefixe);
  }
  static public function getPrefixs() { return self::$lstPrefix; }
  static public function getCls($mbar)
  {
    foreach(self::$lstClass as $cls => $arr_Regex)
      foreach($arr_Regex as $id => $regex)
        if(preg_match($regex, $mbar, $matches))
          return new $cls($id, $matches);
    return false;
  }
  static public function getCls_sugg($mbar)
  {
    foreach(self::$lstClass_sugg as $cls => $arr_Regex)
      foreach($arr_Regex as $id => $regex)
        if(preg_match($regex, $mbar, $matches))
          return new $cls($id, $matches);
    return false;
  }
  static protected function location($url)
  {
    header('Location: '.$url);
    exit;
  }
  static protected function urlGet($url, $arr_param)
  {
    foreach($arr_param as $k => &$v)
    {
      $v = urlencode($k).'='.urlencode($v);
    }
    return $url.'?'.implode('&', $arr_param);
  }
  static protected function locationGet($url, $arr_param)
  {
    $url = self::urlGet($url, $arr_param);
    self::location($url);
  }
  static protected function locationPost($url, $arr_param)
  {
    header('Content-type: text/html');
?>
<!doctype html>
<html>
  <head>
    <title>Redirect to search</title>
  </head>
  <body onload="document.getElementById('search').submit()">
    Redirection in running
    <form id="search" method="POST" action="<?=$url?>">
<?php
    foreach($arr_param as $k => $v)
    {
      echo '<input type="hidden" name="'.htmlentities($k).'" value="'.htmlentities($v).'" />';
    }
?>
      <input type="submit" value="Click if you don't redirect" />
    </form>
  </body>
</html>
<?php
    exit;
  }
  static protected function locationGetPost($url, $arr_param, $arr_param_post)
  {
    $url = self::urlGet($url, $arr_param);
    self::locationPost($url, $arr_param_post);
  }
  private $cache = null;
  private function loadCache()
  {
    $file = 'cache/'.get_class($this).'.cache';
    if(is_file($file)) $this->cache = unserialize(file_get_contents($file));
    else $this->cache = [];
  }
  private function saveCache()
  {
    $file = 'cache/'.get_class($this).'.cache';
    return file_put_contents($file, serialize($this->cache));
  }
  protected function getCache($elem)
  {
    $cache_s = $this->cache_sec;
    $this->loadCache();
    if(isset($this->cache[$elem])
      && $this->cache[$elem]['time'] > time() - $cache_s)
      return $this->cache[$elem]['data'];
    else
      $this->clearCache($cache_s);
    return false;
  }
  protected function setCache($elem, $data)
  {
    if($this->cache === null) $this->loadCache();
    $this->cache[$elem]['time'] = time();
    $this->cache[$elem]['data'] = $data;
    return $this->saveCache();
  }
  protected function clearCache()
  {
    $cache_s = $this->cache_sec;
    if($this->cache === null) $this->loadCache();
    $time_max = time() - $cache_s;
    foreach(array_keys($this->cache) as $i)
    {
      if($this->cache[$i]['time'] < $time_max)
        unset($this->cache[$i]);
    }
    return $this->saveCache();
  }
}

foreach(scandir('mod/') as $i)
  if(preg_match('~^[a-zA-Z0-9_-]+\.cls.php$~', $i))
    require_once 'mod/'.$i;

if($mbar !== false)
{
  $cls = act::getCls($mbar);
  if($cls) $cls->exec();
  else die('Magic is unknow');
}
elseif($msugg !== false)
{
  $cls = act::getCls_sugg($msugg);
  #header('Content-type: application/x-suggestions+json');
  header('Content-type: text/html; charset=utf8');
  if($cls) echo(json_encode([$msugg, $cls->sugg(),[],[]]));
  else
  {
    $pref = act::getPrefixs();
    sort($pref);
    echo(json_encode([$msugg, $pref,[],[]]));
  }
}






