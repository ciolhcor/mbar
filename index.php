<!doctype html>
<html>
  <head>
    <title>Magique bar</title>
    <link rel="search" type="application/opensearchdescription+xml" href="opensearch.xml" title="Magique bar CiHr"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
var MIN_LENGTH = 0;
$( document ).ready(function() {
	$("#keyword").keyup(function() {
		var keyword = $("#keyword").val();
		if (keyword.length >= MIN_LENGTH) {
			$.get( "act.php", { msugg: keyword } )
			  .done(function( data ) {
          var results = jQuery.parseJSON(data);
          console.log(keyword);
          console.log(keyword);
          if (keyword == $("#keyword").val())
          {
            $('#results').html('');
            $(results[1]).each(function(key, value) {
              $('#results').append('<div class="item">' + value + '</div>');
            })
            $('.item').click(function() {
              var text = $(this).text();
              $('#keyword').val(text);
              /* $('#myform').submit(); /**/
            })
          }
			  });
		} else {
			$('#results').html('');
		}
	});

    $("#keyword").blur(function(){
    		$("#results").fadeOut(500);
    	})
        .focus(function() {		
    	    $("#results").show();
    	});
});
</script>
<style>
#keyword {
  width: 80%;
	font-size: 1em;
}

#results {
	width: 80%;
	position: absolute;
	border: 1px solid #c0c0c0;
}

#results .item {
	padding: 3px;
	font-family: Helvetica;
	border-bottom: 1px solid #c0c0c0;
}

#results .item:last-child {
	border-bottom: 0px;
}

#results .item:hover {
	background-color: #f2f2f2;
	cursor: pointer;
}
</style>
  </head>
  <body>
    <form method="post" action="act.php" id="myform">
      <input id="keyword" type="text" autofocus name="mbar" autocomplete=off placeholder="Magique bar"/>
      <input type="submit" />
    </form>
<div id="results">
</div>
  </body>
</html>
