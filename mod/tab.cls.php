<?php
/**
 * @file mod/tab.cls.php
 * @version 1.0.1
 * @author Ciol Hcor
 */

class act_tab extends act {
  static public function addCls() { self::addAct(__CLASS__, ['tab '], self::$regex, self::$regex_sugg); }
  protected $cache_sec= 600;

  static private $regex = [
    'numeric' => '~^tab *(?<num>[0-9]+(\.[0-9]+)?)( .+)?$~',
    'other' => '~^tab (?<num>.+)$~',
  ];
  static private $regex_sugg = [
    'all' => '~^tab (?<num>.+)$~',
  ];

  protected function exec_numeric()
  {
    $url = 'http://www.tableaux-horaires.ch/fileadmin/fap_pdf_fields/'
      .date('Y')
      .'/'
      .$this->regexMatches['num']
      .'.pdf';
    if(@fclose(@fopen($url, 'r'))) {
      header('Location: '.$url);
      exit;
    }
    else $this->exec_other();
  }
  protected function exec_other()
  {
    $url = 'http://www.tableaux-horaires.ch/fr/bienvenue.html';
    $param['tx_fapch_pi1[sword]'] = $this->regexMatches['num'];
    self::locationGet($url, $param);
  }
  protected function sugg_all()
  {
    $cache = $this->getCache($this->regexMatches['num']);
    if($cache !== false)
      return $cache;

    $prefix = 'tab ';
    $url = 'http://www.tableaux-horaires.ch/fr/bienvenue.html';
    $param['tx_fapch_pi1[sword]'] = $this->regexMatches['num'];
    $file = file_get_contents(self::urlGet($url, $param));
    $dom = new DOMDocument;
    $dom->loadHTML($file);
    $xpath = new DOMXPath($dom);
    $data = $xpath->evaluate('//div[@class="tx-fapch-pi1-listrow"]/table/tr/td[@valign="top"][1]/a');
    $data_ret = [];
    foreach($data as $i)
      $data_ret[] = $prefix.$i->firstChild->C14N();
    $j=0;
    $data = $xpath->evaluate('//div[@class="tx-fapch-pi1-listrow"]/table/tr/td[@valign="top"][3]');
    foreach($data as $i)
      $data_ret[$j++] .= ' '.$i->firstChild->C14N();
    $this->setCache($this->regexMatches['num'], $data_ret);
    return $data_ret;
  }
}
act_tab::addCls();
