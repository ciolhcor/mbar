<?php
/**
 * @file mod/cff.cls.php
 * @version 0.1.1
 * @author Ciol Hcor
 */

class act_cff extends act {
  static public function addCls() { self::addAct(__CLASS__, ['cff ', 'sbb ', 'ffs '], self::$regex, self::$regex_sugg); }
  protected $cache_sec = 600;

  static private $regex = [
    #'numeric' => '~^tab *(?<num>[0-9]+(\.[0-9]+)?)( .+)?$~',
    #'other' => '~^tab (?<num>.+)$~',
    'ville' => '~^(?<pref>(sbb|cff|ffs))\s*(?<from>[^>]+)\s*>\s*(?<vias>([^>]+\s*>\s*)*)(?<to>[^>]+)\s*$~',
  ];
  static private $regex_sugg = [
    'ville' => '~^(?<pref>(sbb|cff|ffs)\s*)(?<prearret>(([^>]+)\s*[>]\s*)*)(?<mot>[^>]+)$~',
  ];

  protected function exec_ville()
  {
    $pref = $this->regexMatches['pref'];
    $from = $this->regexMatches['from'];
    $to = $this->regexMatches['to'];
    $vias = explode('>', trim($this->regexMatches['vias'], "> \t\n\r\0\x0B"));

    $url = 'http://fahrplan.sbb.ch/bin/query.exe/fn?OK#focus';
    $url = 'http://fahrplan.sbb.ch/bin/query.exe/fn';
    $param = [];


    $param['start'] = 'yes';

    #$param['date'] = '11.08.16';
    #$param['REQ0JourneyTime'] = '13:00';
    #$param['REQ0HafasSearchForw'] = '1';
    #$param['REQ0HafasSearchForw'] = '0';

    $param['REQ0JourneyStopsS0G'] = $from;
    $param['REQ0JourneyStopsS0A'] = '255';

    $param['REQ0JourneyStopsZ0G'] = $to;
    $param['REQ0JourneyStopsZ0A'] = '255';

    $i = 1;
    foreach($vias as $via)
    {
      $param["REQ0JourneyStops$i.0G"] = $via;
      $param["REQ0JourneyStops$i.0A"] = 255;
      $i++;
    }

    self::locationPost($url, $param);

  }
  protected function sugg_ville()
  {
    $search = $this->regexMatches['mot'];
    $prefix = $this->regexMatches['pref'];
    $prearret = $this->regexMatches['prearret'];
    $cache = $this->getCache($search);
    if($cache !== false)
      return array_map(function ($v) use($prefix, $prearret) { return $prefix.$prearret.$v; }, $cache);


    $url = 'http://fahrplan.sbb.ch/bin/ajax-getstop.exe/fny';
    $param['encoding'] = 'utf-8';
    $param['REQ0JourneyStopsS0A'] = 7;
    $param['REQ0JourneyStopsS0A'] = 0b011; # 0b100 = POI; 0b010 = Address ; 0b001 = Gare
    $param['REQ0JourneyStopsS0G'] = $search;
    $file = file_get_contents(self::urlGet($url, $param));
    $file = substr($file, 8);
    $file = substr($file, 0, -22);
    
    $data = json_decode($file);
    $data_ret = [];

    foreach($data->suggestions as $i)
      $data_ret[] = $i->value;

    $this->setCache($search, $data_ret);
    return array_map(function ($v) use($prefix, $prearret) { return $prefix.$prearret.$v; }, $data_ret);
    return $data_ret;
  }
}
act_cff::addCls();
